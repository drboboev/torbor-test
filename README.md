## Работа с классом

Подключаем файл к проекту:

```
#!php
require_once "path/to/Difference.php";
```

Создаём экземпляр класса и передаём параметры:

```
#!php
$difference = new Difference($allowedDiff, $currentPrice, $previousPrice);
```

Вызываем метод diff():

```
#!php
$difference->diff();
```

Получаем результат:

```
#!php
$result = $difference->getAmount();
```


#### Пример работы с классом:

```
#!php
require_once "path/to/Difference.php";

$difference = new Difference(10, 15, 20);
$difference->diff();

$result = $difference->getAmount();
```

Значение переменной $result будет равно 25%