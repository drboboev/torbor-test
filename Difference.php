<?php

/**
 * Class Difference
 *
 * Класс для вычисления отклонения между текущей ценой и предыдущей
 */
class Difference
{
    /**
     * @var int
     *
     * результат вычислений класса
     */
    private $amount;

    /**
     * @var int
     *
     * разрешённое отклонение цены
     * передаётся при инициализации класса
     */
    private $allowedDiff;

    /**
     * @var int
     *
     * текущая цена
     * передаётся при инициализации класса
     */
    private $currentPrice;

    /**
     * @var int
     *
     * предыдущая цена
     * передаётся при инициализации класса
     */
    private $previousPrice;

    public function __construct(int $allowedDiff, int $currentPrice, int $previousPrice = 0)
    {
        $this->allowedDiff = $allowedDiff;
        $this->currentPrice = $currentPrice;
        $this->previousPrice = $previousPrice;
    }

    // метод, вычисляющий отклонение текущей цены от предыдущей
    public function diff(): bool
    {
        if ($this->previousPrice == 0) {
            $result = 0;
        } else {
            $result = $this->calculateDiff();
        }

        $this->setAmount(abs($result));

        if ($this->amount > $this->allowedDiff)
            return false;

        return true;
    }

    // метод рассчитывает отклонение между текущей и предыдущей ценой
    private function calculateDiff(): int
    {
        return (($this->currentPrice - $this->previousPrice) / $this->previousPrice) * 100;
    }

    // метод записывает значение $amount в свойство объекта
    private function setAmount($amount)
    {
        $this->amount = $amount;
    }

    // метод возвращает результат функции diff() записанный в приватное свойство $amount
    public function getAmount(): int
    {
        return $this->amount;
    }
}
